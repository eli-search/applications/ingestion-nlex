import datetime
import logging
import uuid
from typing import List

import pytz

from src.common import utils
from src.common.aws_clients import dynamodb_resource, dynamodb_client
from src.common.aws_clients import s3_client
from src.common.aws_clients import sqs_client
from src.common.aws_clients import ssm_client
from src.common.config import SQS_MAX_BATCH_SIZE
from src.common.exceptions import GetSSMException, TableNotFoundException
from src.common.exceptions import S3UploadException
from src.common.exceptions import ScanTableException
from src.common.exceptions import SqsInsertionException
from src.common.models import NoticeType
from src.common.models import ResourceType
from src.common.utils import get_env_var_value


def get_ssm_parameter(path: str) -> str:
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    try:
        parameter = ssm_client().get_parameter(Name=path)
        table_name = parameter['Parameter']['Value']
        logger.info(f"Parameter : '{path}' retrieved from SSM")
        return table_name
    except Exception as err:
        raise GetSSMException(f"Get SSM Exception when retrieving parameter : '{path}'."
                              f" Exception : {err}") from err


def get_ingestion_checkpoint(ingestion_type: str) -> str:
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    table_name = get_env_var_value("INGESTION_CHECKPOINT_TABLE_NAME")
    try:
        table = get_table_resource(table_name)
        response = table.get_item(Key={'ingestion_type': ingestion_type})
        checkpoint = response['Item']['last_ingestion_date']
        logger.info(f'Injection checkpoint retrieved : {checkpoint}')
        return checkpoint
    except TableNotFoundException as err:
        raise err
    except KeyError as err:
        logger.warning(f'Ingestion checkpoint not found : {err}')
        return ''


def get_table_resource(table_name: str):
    client = dynamodb_client()
    try:
        client.describe_table(TableName=table_name)
        table = dynamodb_resource().Table(table_name)
        return table
    except client.exceptions.ResourceNotFoundException as err:
        raise TableNotFoundException(f'Table {table_name} does not exist or is being created: {err}')


def update_ingestion_checkpoint(ingestion_type: str, date: str):
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    table_name = get_env_var_value("INGESTION_CHECKPOINT_TABLE_NAME")
    try:
        table = get_table_resource(table_name)
        table.put_item(Item={'ingestion_type': ingestion_type, 'last_ingestion_date': date})
        logger.info(f'Ingestion checkpoint for key={ingestion_type} updated to : {date}')
    except TableNotFoundException as err:
        raise err
    except Exception as err:
        logger.error(f'Error while updating ingestion checkpoint: {err}')


def insert_in_queue(entries: List[dict], queue_url: str) -> None:
    try:
        sqs_client().send_message_batch(QueueUrl=queue_url, Entries=entries)
    except Exception as err:
        raise SqsInsertionException(f"Error while inserting in the SQS queue: {err}") from err


def insert_batches_in_queue(resource_ids: List[str], resource_type: ResourceType) -> None:
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    queue_url = get_env_var_value("INGESTION_QUEUE_URL")
    batches = [resource_ids[x:x + SQS_MAX_BATCH_SIZE] for x in range(0, len(resource_ids), SQS_MAX_BATCH_SIZE)]
    logger.info(f'Inserting {len(resource_ids)} {resource_type.value}(s) in SQS queue : {queue_url}')
    for batch in batches:
        entries = []
        for resource_id in batch:
            entry = {'Id': str(uuid.uuid4()),
                     'MessageAttributes': {'type': {'StringValue': resource_type.value, 'DataType': 'String'}},
                     'MessageBody': resource_id}
            entries.append(entry)
        insert_in_queue(entries, queue_url)


def write_file_in_s3(bucket: str, key: str, content: str) -> None:
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    try:
        logger.info(f"Upload s3://{bucket}/{key}...")
        s3_client().put_object(Body=content, Bucket=bucket, Key=key)
        logger.info(f"s3://{bucket}/{key} uploaded")
    except Exception as err:
        raise S3UploadException(f"Failed to upload S3 object 's3://{bucket}/{key}': {err}") from err


def insert_resources_in_ingestion_table(resources: str | List[str], resource_type: ResourceType):
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    table_name = get_env_var_value("INGESTION_TASKS_TABLE_NAME")

    try:
        table = get_table_resource(table_name)
        if resources:
            if isinstance(resources, str):
                if resource_type == ResourceType.NOTICE:
                    resources = utils.normalize_notice_id(resources)
                table.put_item(Item={
                    'resource_id': resources,
                    'resource_type': resource_type.value
                })
                logger.info(f'{resource_type.value} : {resources} append in the DynamoDB ingestion table')
            elif isinstance(resources, list):
                with table.batch_writer() as writer:
                    for resource in resources:
                        if resource_type == ResourceType.NOTICE:
                            resource = utils.normalize_notice_id(resource)
                        writer.put_item(Item={'resource_id': resource, 'resource_type': resource_type.value})
                logger.info(f'{resource_type.value}(s) : {resources} append in the DynamoDB ingestion table')
            else:
                raise Exception(f"Resource type not supported of insertion in ingestion table, got {type(resources)}")
        else:
            logger.info(f'Nothing to append in the DynamoDB ingestion table')
    except TableNotFoundException as err:
        raise err
    except Exception as e:
        logger.error(f'Impossible to insert {resource_type.value}(s) : {resources} into the DynamoDB ingestion table : {e}')


def extract_resource_id_from_items(items: List) -> List:
    result = list()
    for item in items:
        result.append(item['resource_id'])
    return result


def scan_ingestion_tasks_table(resource_type: ResourceType) -> List[str]:
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    table_name = get_env_var_value("INGESTION_TASKS_TABLE_NAME")
    resource_ids = list()
    try:
        table = get_table_resource(table_name)
        response = table.scan(ScanFilter={
                'resource_type': {'AttributeValueList': [resource_type.value],
                                  'ComparisonOperator': 'EQ'}})
        resource_ids.extend(extract_resource_id_from_items(response.get("Items", [])))
        while response.get('LastEvaluatedKey'):
            response = table.scan(ExclusiveStartKey=response['LastEvaluatedKey'], ScanFilter={
                'resource_type': {'AttributeValueList': [resource_type.value],
                                  'ComparisonOperator': 'EQ'}})
            resource_ids.extend(extract_resource_id_from_items(response.get("Items", [])))
        logger.info(f"{len(resource_ids)} {resource_type.value}s to ingest from DynamoDB")
        return resource_ids
    except TableNotFoundException as err:
        raise err
    except Exception as err:
        raise ScanTableException(f"Scan Table Exception: {err}") from err


def remove_resources_from_ingestion_table(resource: str):
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    table_name = get_env_var_value("INGESTION_TASKS_TABLE_NAME")
    if resource:
        logger.info(f"Removing {resource} from DynamoDB table {table_name}")
        try:
            table = get_table_resource(table_name)
            table.delete_item(Key={"resource_id": resource})
            logger.info(f'Resource(s) : {resource} removed from DynamoDB ingestion table')
        except TableNotFoundException as err:
            raise err
        except Exception as e:
            logger.error(f'Impossible to delete resource : {resource} from DynamoDB ingestion table : {e}')
    else:
        logger.info('Nothing to remove from DynamoDB ingestion table')


def insert_resource_in_reference_table(notice_id: str, contract_id: str, notice_type: NoticeType) -> None:
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    table_name = get_env_var_value("INGESTION_REFERENCES_TABLE_NAME")
    try:
        table = get_table_resource(table_name)
        table.put_item(TableName=table_name, Item={'notice_id': notice_id, 'contract_id': contract_id,
                                                   'notice_type': notice_type.value})
        logger.info(f'Reference {contract_id} for {notice_type.value}: {notice_id} '
                    f'added to the ingestion references table')
    except TableNotFoundException as err:
        raise err
    except Exception as err:
        logger.error(f'Error while adding item in ingestion references table: {err}')


def get_contract_id_in_reference_table(notice_id: str) -> str:
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    table_name = get_env_var_value("INGESTION_REFERENCES_TABLE_NAME")
    try:
        table = get_table_resource(table_name)
        logger.info(f"Retrieving contract ID reference linked to notice ID: {notice_id}")
        response = table.get_item(Key={'notice_id': notice_id})
        contract_id = response['Item']['contract_id']
        logger.info(f'Contract ID {contract_id} retrieved for notice : {notice_id}')
        return contract_id
    except TableNotFoundException as err:
        raise err
    except Exception as err:
        logger.warning(f'Contract ID not found in reference table: {err}')
        return ''


def get_queue_messages_status():
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    queue_url = get_env_var_value("INGESTION_QUEUE_URL")
    response = sqs_client().get_queue_attributes(
        QueueUrl=queue_url,
        AttributeNames=[
            'ApproximateNumberOfMessages',
            'ApproximateNumberOfMessagesNotVisible',
            'ApproximateNumberOfMessagesDelayed'
        ]
    )
    attributes = response.get("Attributes", {})
    if attributes:
        logger.info(f"Retrieved the following queue attributes: {attributes}")
    else:
        logger.info(f"No attributes returned from {queue_url}")
    return attributes


def update_ingestion_task_with_status(resource_id: str, status_message: str):
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    table_name = get_env_var_value("INGESTION_TASKS_TABLE_NAME")
    logger.info(f"Updating status of {resource_id} from DynamoDB table {table_name} with '{status_message}'")
    try:
        table = get_table_resource(table_name)
        table.update_item(
            Key={"resource_id": resource_id},
            UpdateExpression="set ingestion_status = :is,  last_updated_timestamp = :ts",
            ExpressionAttributeValues={
                ':is': status_message,
                ':ts': str(datetime.datetime.now(tz=pytz.timezone("Europe/Brussels")))
            }
        )
    except TableNotFoundException as err:
        raise err
    except Exception as e:
        logger.error(f'Impossible to update resource : {resource_id} from DynamoDB ingestion table : {e}')
