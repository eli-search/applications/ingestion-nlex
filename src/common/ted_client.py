import json
import logging

import requests

from src.common.exceptions import TedApiException
from src.common.utils import get_env_var_value


def get_notices_from_ted_api(filters: str) -> json:
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    url = f"""https://ted.europa.eu/api/v3.0/notices/search?fields={filters}"""
    try:
        logger.info(f'Calling TED API : GET - {url}')
        response = requests.request("GET", url)
        return json.loads(response.text)
    except Exception as err:
        raise TedApiException(f"Error reaching TED API: {err}") from err
