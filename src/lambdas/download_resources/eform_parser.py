import logging
import os
from dataclasses import asdict

from lxml import etree
from lxml.etree import _Element
from pprint import pprint

from src.common import utils
from src.common.models import IngestionNotice
from src.common.utils import normalize_notice_id, is_valid_etendering_url

NOTICE_ID_XPATH = "//efbc:NoticePublicationID/text()"
NOTICE_TYPE_XPATH = "//cbc:NoticeTypeCode/text()"
PUB_DATE_XPATH = "//efbc:PublicationDate/text()"
REF_NOTICE_ID_XPATH = "//cac:NoticeDocumentReference/cbc:ID/text()"
ETENDERING_URL_XPATH = "//cbc:AccessToolsURI/text()"


class EformNoticeParser:
    xml: _Element
    namespaces: dict

    def __init__(self, xml: _Element):
        self.xml = xml
        ns = get_namespace(xml)
        nsmap = xml.nsmap
        new_nsmap = nsmap | ns
        if None in new_nsmap.keys():
            del new_nsmap[None]
        self.namespaces = new_nsmap
        self.logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))

    def parse(self) -> dict:
        notice_id = self.get_notice_id()
        notice_type = self.get_notice_type()
        notice_publication_date = self.get_pub_date()
        notice_reference_notice_id = self.get_ref_notice_id()
        etendering_url = self.get_etendering_url()
        notice = IngestionNotice(
                id=notice_id,
                type=notice_type,
                publication_date=notice_publication_date,
                reference_notice_id=notice_reference_notice_id,
                namespaces=self.namespaces,
                etendering_url=etendering_url,
                xml_str=""
        )
        self.logger.info(f'Finished parsing the notice.')
        return asdict(notice)

    def get_notice_id(self) -> str:
        self.logger.info(f"Retrieving notice ID with XPATH: {NOTICE_ID_XPATH}")
        notice_id = self.xml.xpath(NOTICE_ID_XPATH, namespaces=self.namespaces)
        return normalize_notice_id(notice_id[0]) if len(notice_id) else ''

    def get_pub_date(self) -> str:
        self.logger.info(f"Retrieving publication date with XPATH: {PUB_DATE_XPATH}")
        date = self.xml.xpath(PUB_DATE_XPATH, namespaces=self.namespaces)
        return date[0] if len(date) else ''

    def get_ref_notice_id(self) -> str:
        self.logger.info(f"Retrieving reference notice ID with XPATH: {REF_NOTICE_ID_XPATH}")
        ref = self.xml.xpath(REF_NOTICE_ID_XPATH, namespaces=self.namespaces)
        return normalize_notice_id(ref[0]) if len(ref) else ''

    def get_notice_type(self) -> str:
        self.logger.info(f"Retrieving notice type with XPATH: {NOTICE_TYPE_XPATH}")
        notice_type = self.xml.xpath(NOTICE_TYPE_XPATH, namespaces=self.namespaces)
        return notice_type[0] if len(notice_type) else ''

    def get_etendering_url(self) -> str:
        self.logger.info(f"Retrieving eTendering URL with XPATH: {ETENDERING_URL_XPATH}")
        url_result = self.xml.xpath(ETENDERING_URL_XPATH, namespaces=self.namespaces)
        url = url_result[0] if len(url_result) else ''
        return url if is_valid_etendering_url(url) else ''


def get_namespace(xml: _Element) -> dict:
    logger = None
    try:
        logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    except:
        pass
    try:
        if "urn" in xml.nsmap.keys():
            return {'urn': xml.nsmap["urn"]}
        elif None in xml.nsmap.keys():
            return {'urn': xml.nsmap[None]}
        else:
            raise KeyError(f"Unhandled case for XML namespace: {xml.nsmap}")
    except KeyError:
        if logger:
            logger.info('Namespace not found')
        return {}


if __name__ == '__main__':
    os.environ["LOGGER_NAME"] = "eform_parser"
    print(f"Contract notice eforms:")
    with open('../../../data/eforms/contract_notice.xml') as f:
        content = f.read()
        xml = etree.fromstring(bytes(content, encoding='utf-8'))
        print(f"Extracted the following namespaces:")
        pprint(xml.nsmap)
        namespace = get_namespace(xml)
        try:
            if namespace:
                parser = EformNoticeParser(xml)
                parsed_notice = parser.parse()
                if parsed_notice:
                    print(f"eForm notice successfully parsed.")
                    pprint(parsed_notice)
                else:
                    print(f"Could not parse notice")
        except Exception as err:
            print(f'Error during parsing : {err}')

    print(f"\nprior information notice eforms:")
    with open('../../../data/eforms/pin.xml') as f:
        content = f.read()
        xml = etree.fromstring(bytes(content, encoding='utf-8'))
        print(f"Extracted the following namespaces:")
        pprint(xml.nsmap)
        namespace = get_namespace(xml)
        try:
            if namespace:
                parser = EformNoticeParser(xml)
                parsed_notice = parser.parse()
                if parsed_notice:
                    print(f"eForm notice successfully parsed.")
                    pprint(parsed_notice)
                else:
                    print(f"Could not parse notice")
        except Exception as err:
            print(f'Error during parsing : {err}')

    print(f"\nContract award notice eforms:")
    with open('../../../data/eforms/can.xml') as f:
        content = f.read()
        xml = etree.fromstring(bytes(content, encoding='utf-8'))
        print(f"Extracted the following namespaces:")
        pprint(xml.nsmap)
        namespace = get_namespace(xml)
        try:
            if namespace:
                parser = EformNoticeParser(xml)
                parsed_notice = parser.parse()
                if parsed_notice:
                    print(f"eForm notice successfully parsed.")
                    pprint(parsed_notice)
                else:
                    print(f"Could not parse notice")
        except Exception as err:
            print(f'Error during parsing : {err}')

    print(f"\nprior information notice (change) eforms:")
    with open('../../../data/eforms/pin-change.xml') as f:
        content = f.read()
        xml = etree.fromstring(bytes(content, encoding='utf-8'))
        print(f"Extracted the following namespaces:")
        pprint(xml.nsmap)
        namespace = get_namespace(xml)
        try:
            if namespace:
                parser = EformNoticeParser(xml)
                parsed_notice = parser.parse()
                if parsed_notice:
                    print(f"eForm notice successfully parsed.")
                    pprint(parsed_notice)
                else:
                    print(f"Could not parse notice")
        except Exception as err:
            print(f'Error during parsing : {err}')
