import logging

from typing import List, Union

from lxml import etree
from lxml.etree import _Element

from src.common import utils
from src.common.aws_requests import get_contract_id_in_reference_table, update_ingestion_task_with_status
from src.common.aws_requests import insert_resource_in_reference_table
from src.common.aws_requests import insert_resources_in_ingestion_table
from src.common.aws_requests import remove_resources_from_ingestion_table
from src.common.aws_requests import write_file_in_s3
from src.common.config import SCOPE
from src.common.exceptions import TableNotFoundException
from src.common.models import IngestionNotice
from src.common.models import NoticeType
from src.common.models import ResourceType
from src.common.ted_client import get_notices_from_ted_api
from src.common.utils import clean_key
from src.common.utils import join_list_elements_to_string
from src.common.utils import convert_base64_to_str
from src.common.utils import normalize_notice_type
from src.lambdas.download_resources.eform_parser import EformNoticeParser
from src.lambdas.download_resources.notice_parser import NoticeParser


def get_api_filters(notice_ids: List[str]) -> str:
    query = '[' + ' or '.join(notice_id for notice_id in notice_ids) + ']'
    return f'ND,PD,content&scope={SCOPE}&q=ND={query}'


def get_notice_output_path(publication_date: str, contract_id: str, notice_id: str,
                           notice_type: NoticeType, notice_format) -> str:
    logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    logger.info(f'Extracting date from publication date: {publication_date}')
    if "-" in publication_date:
        publication_date = ''.join(filter(str.isdigit, publication_date))
    year = publication_date[0:4]
    month = publication_date[4:6]
    day = publication_date[6:8]

    logger.info(f'Extracted year: {year}, month: {month}, day: {day}')

    filename = f'{notice_id}.{notice_format}'
    logger.info(f'Creating path with filename: {filename}')
    return f'resource_type={notice_type.value}' \
           f'/format={notice_format}' \
           f'/year={year}' \
           f'/month={month}' \
           f'/day={day}' \
           f'/contract_id={contract_id}/{filename}'


def insert_value(nodes: List[str], flattened_dict: dict, value: str):
    path = join_list_elements_to_string(nodes, '.')
    flattened_dict[path] = str(value) if value else 'null'
    nodes.pop()
    return nodes


def handle_list(key: str, nodes: List, flattened_dict: dict, list_element: List):
    values = []
    change_count = 0
    quote_count = 0
    for element in list_element:
        if isinstance(element, dict):
            if key in ["ORIGINAL_CPV", "CPV_ADDITIONAL"]:
                nodes = handle_cpvs(key, nodes, flattened_dict, list_element)
            else:
                if '@LG' in element:
                    nodes.append(element['@LG'])
                elif '@ITEM' in element:
                    label = element['@ITEM']
                    nodes.append(f'ITEM_{label}')
                elif '@VALUE' in element:
                    nodes.append(element['@VALUE'])
                elif '@PUBLICATION' in element:
                    nodes.append(f'CHANGE_{change_count}')
                    change_count += 1
                elif '@QUOTE' in element:
                    nodes.append(f'QUOTE_{change_count}')
                    quote_count += 1
                if isinstance(element, dict):
                    custom_dict_flattener(element, nodes, flattened_dict)
        elif isinstance(element, str):
            values.append(element)
    if len(values):
        nodes.append(key)
        nodes = insert_value(nodes, flattened_dict, join_list_elements_to_string(values, ','))
    return nodes


def handle_cpvs(key: str, nodes: list, flattened_dict: dict, value: list):
    codes = []
    if isinstance(value, list):
        for element in value:
            if '@CODE' in element:
                codes.append(element['@CODE'])
            if 'CPV_CODE' in element:
                codes.append(element['CPV_CODE']['@CODE'])
        nodes.append(key)
        nodes = insert_value(nodes, flattened_dict, join_list_elements_to_string(codes, ','))
    else:
        print('Case not handled', type(value))
    return nodes


def custom_dict_flattener(data_dict: dict, nodes: list, flattened_dict: dict):
    for key in data_dict.keys():
        value = data_dict[key]
        if isinstance(value, dict):
            nodes.append(clean_key(key))
            custom_dict_flattener(value, nodes, flattened_dict)
        elif isinstance(value, list):
            nodes = handle_list(key, nodes, flattened_dict, value)
        else:
            nodes.append(clean_key(key))
            nodes = insert_value(nodes, flattened_dict, value)
    if nodes:
        nodes.pop()


def write_xml_notice_to_s3(input_bucket: str, notice_id: str, contract_id: str,
                           notice_str: str, notice_type: NoticeType, publication_date: str) -> None:
    logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    xml_path = get_notice_output_path(publication_date, contract_id, notice_id, notice_type, 'xml')
    logger.info(f'XML path for S3: {xml_path}')
    write_file_in_s3(input_bucket, xml_path, notice_str)
    logger.info('Finished writing file to S3')


def get_parser(xml: _Element) -> Union[EformNoticeParser, NoticeParser]:
    logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    logger.info(f'Trying to find parser with ns map: {xml.nsmap}')

    if any("http://publications.europa.eu/TED_schema/Export" in value for value in xml.nsmap.values()):
        return NoticeParser(xml)
    if any("ted/" in value for value in xml.nsmap.values()):
        return NoticeParser(xml)
    if any("urn:oasis:names:specification:ubl:schema:xsd:" in value for value in xml.nsmap.values()):
        return EformNoticeParser(xml)

    raise Exception(f"Could not find parser to use for notice: {xml}")


def process_contract_notices(contract_notices: List[dict]) -> None:
    logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    logger.info(f'Processing {len(contract_notices)} Contract Notice(s)')
    input_bucket = utils.get_env_var_value("INPUT_BUCKET")
    for notice in contract_notices:
        notice_id = notice["id"]
        logger.info(f'Processing Contract Notice with id : {notice_id}')
        try:
            etendering_url = notice["etendering_url"]
            if etendering_url:
                insert_resources_in_ingestion_table(etendering_url, ResourceType.PROCUREMENT)
            pin_id = notice["reference_notice_id"]
            contract_id = notice_id
            if pin_id:
                contract_id = pin_id
                insert_resource_in_reference_table(pin_id, contract_id, NoticeType.PRIOR_INFORMATION_NOTICE)
            insert_resource_in_reference_table(notice_id, contract_id, NoticeType.CONTRACT_NOTICE)

            write_xml_notice_to_s3(
                    input_bucket,
                    notice_id,
                    contract_id,
                    notice["xml_str"],
                    NoticeType.CONTRACT_NOTICE,
                    notice["publication_date"]
            )
            remove_resources_from_ingestion_table(notice_id)
        except Exception as err:
            update_ingestion_task_with_status(notice_id, str(err))
            logger.error(f'Error while writing XML notice and parquet to S3 : {err}')


def process_prior_notices(prior_notices: List[dict]) -> None:
    logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    logger.info(f'Processing {len(prior_notices)} Prior Information Notice(s)')
    input_bucket = utils.get_env_var_value("INPUT_BUCKET")
    for notice in prior_notices:
        notice_id = notice["id"]
        logger.info(f'Processing Prior Information Notice with id : {notice_id}')
        try:
            insert_resource_in_reference_table(notice_id, notice_id, NoticeType.PRIOR_INFORMATION_NOTICE)
            write_xml_notice_to_s3(
                    input_bucket,
                    notice_id,
                    notice_id,
                    notice["xml_str"],
                    NoticeType.PRIOR_INFORMATION_NOTICE,
                    notice["publication_date"]
            )
            remove_resources_from_ingestion_table(notice_id)
        except Exception as err:
            update_ingestion_task_with_status(notice_id, str(err))
            logger.error(f'Error while writing XML notice and parquet to S3 : {err}')


def process_award_notices(award_notices: List[dict]) -> None:
    logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    logger.info(f'Processing {len(award_notices)} Contract Award Notice(s)')
    input_bucket = utils.get_env_var_value("INPUT_BUCKET")
    for notice in award_notices:
        notice_id = notice['id']
        logger.info(f'Processing Contract Award Notice with id : {notice_id}')
        try:
            reference_id = notice["reference_notice_id"]
            contract_id = get_contract_id_in_reference_table(reference_id)
            if contract_id:
                insert_resource_in_reference_table(notice_id, contract_id, NoticeType.CONTRACT_AWARD_NOTICE)
            else:
                logger.warning(f'Contract ID not found in the XML, using the contract ID "unknown"')
                contract_id = 'unknown'
            write_xml_notice_to_s3(
                    input_bucket,
                    notice_id,
                    contract_id,
                    notice["xml_str"],
                    NoticeType.CONTRACT_AWARD_NOTICE,
                    notice["publication_date"]
            )
            remove_resources_from_ingestion_table(notice_id)
        except Exception as err:
            update_ingestion_task_with_status(notice_id, str(err))
            logger.error(f'Error while writing XML notice and parquet to S3 : {err}')


def process_change_notices(change_notices: List[dict]) -> None:
    logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    logger.info(f'Processing {len(change_notices)} Change Notice(s)')
    input_bucket = utils.get_env_var_value("INPUT_BUCKET")
    for notice in change_notices:
        notice_id = notice["id"]
        logger.info(f'Processing Change Notice with id : {notice_id}')
        try:
            reference_id = notice["reference_notice_id"]
            contract_id = get_contract_id_in_reference_table(reference_id)
            if contract_id:
                insert_resource_in_reference_table(notice_id, contract_id, NoticeType.CHANGE_NOTICE)
            else:
                logger.warning(f'Contract ID not found in the XML, using the contract ID "unknown"')
                contract_id = 'unknown'

            write_xml_notice_to_s3(
                    input_bucket,
                    notice_id,
                    contract_id,
                    notice["xml_str"],
                    NoticeType.CHANGE_NOTICE,
                    notice["publication_date"]
            )
            remove_resources_from_ingestion_table(notice_id)
        except Exception as err:
            update_ingestion_task_with_status(notice_id, str(err))
            logger.error(f'Error while writing XML notice and parquet to S3 : {err}')


def process_notice_ids(notice_ids: List[str]):
    logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    logger.info(f'Start processing {len(notice_ids)} notices.')
    api_filters = get_api_filters(notice_ids)
    contract_notices, prior_notices, award_notices, change_notices = [], [], [], []
    notice_id = None
    try:
        response = get_notices_from_ted_api(api_filters)
        logger.info(f'Notices downloaded, started processing of {len(response["results"])}')
        for result in response['results']:
            notice_id = None
            notice_str = convert_base64_to_str(result['content'])
            logger.info('Notice converted from base64 to str successfully.')
            xml = etree.fromstring(bytes(notice_str, encoding='utf-8'))
            logger.info('XML notice parsed successfully.')
            logger.info(f'Get parser for notice')
            parser = get_parser(xml)
            logger.info(f'Extracting information from notice with parser {type(parser)}.')
            ingestion_notice = parser.parse()
            logger.info(f'Notice parsed successfully: {ingestion_notice}')
            logger.info(f'Notice namespaces: {ingestion_notice["namespaces"]}')

            notice_type = ingestion_notice["type"]
            if notice_type:
                logger.info(f"Raw Notice type extracted from XML: {notice_type}")
                notice_type = normalize_notice_type(notice_type)
                logger.info(f'Converted to {notice_type}')
            else:
                logger.error(f"Could not extract notice type: {notice_str}")

            notice_id = ingestion_notice["id"]
            if notice_id:
                logger.info(f"Notice ID extracted: {notice_id}")
            else:
                logger.error(f"Could not extract notice ID from XML : {notice_str}")

            ingestion_notice["xml_str"] = notice_str

            if notice_type == NoticeType.CONTRACT_NOTICE:
                contract_notices.append(ingestion_notice)
            elif notice_type == NoticeType.PRIOR_INFORMATION_NOTICE:
                prior_notices.append(ingestion_notice)
            elif notice_type == NoticeType.CONTRACT_AWARD_NOTICE:
                award_notices.append(ingestion_notice)
            elif notice_type == NoticeType.CHANGE_NOTICE:
                change_notices.append(ingestion_notice)
            logger.info(f'Successfully added notice of type {notice_type} to the list to be processed.')
        process_contract_notices(contract_notices)
        process_prior_notices(prior_notices)
        process_award_notices(award_notices)
        process_change_notices(change_notices)
    except TableNotFoundException as err:
        raise err
    except Exception as err:
        if notice_id:
            update_ingestion_task_with_status(notice_id, str(err))
        else:
            logger.info("Processing failed for a notice but the ingestion task cannot be updated because the notice "
                        "ID could not be extracted.")
        logger.error(f'Error while downloading and parsing notices, IDs kept in the ingestion table : {err}')
