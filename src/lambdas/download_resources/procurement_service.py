import logging
import math
from html.parser import HTMLParser
from io import StringIO
from typing import List

import requests
from lxml import etree
from lxml.etree import _Element

from src.common import utils
from src.common.aws_requests import remove_resources_from_ingestion_table
from src.common.aws_requests import write_file_in_s3
from src.common.config import VALID_EXTENSIONS
from src.common.models import ResourceType
from src.common.utils import normalize_notice_id

DOWNLOAD_DOCS_XPATH = """
    //tr[./td[contains(text(), 'Technical specifications') 
    or contains(text(), 'Tender specifications')]]
    /td[./table[@class = 'transparentTable']]//
    td[./a/img[@alt = 'English (en)']]/a[contains(@href, 'download')]/@href"""
NUMBER_OF_DOCS_XPATH = "//span[contains(text(), 'Documents found')]/text()"
HTML_NOTICE_ID_XPATH = """
    //tr[./td[./div[contains(text(), 'Contract notice')]]]
    /td[./a[contains(@id, 'notice_link')]]/a/text()"""


def get_procurement_output_path(filename: str, publication_date: str, contract_id: str) -> str:
    date = publication_date.split('/')
    extension = filename.split('.')[-1]
    return f'resource_type={ResourceType.PROCUREMENT.value}/format={extension}/year={date[2]}/month={date[1]}/day={date[0]}/contract_id={contract_id}/{filename}'


def is_valid_file(file_extension: str) -> bool:
    return any(extension in file_extension for extension in VALID_EXTENSIONS)


def extract_filename_from_response(file: requests.models.Response) -> str:
    content_disposition = file.headers.get('Content-Disposition')
    filename = content_disposition.split('\'')[-1]
    return filename


def download_file_as_bytes(url: str) -> [bytes, str]:
    logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    try:
        r = requests.get(url, allow_redirects=True)
        filename = extract_filename_from_response(r)
        if is_valid_file(filename.split('.')[-1]):
            return r.content, filename
        return b'', ''
    except Exception as err:
        logger.error(f'Error while downloading {url} : {err}')


def get_nb_documents(html: _Element) -> int:
    result = html.xpath(NUMBER_OF_DOCS_XPATH)
    if result:
        text = result[0]
        return int(text.split(' ')[0])
    return 1


def get_document_url(url: str, page_num: int) -> str:
    return f'{url.replace("display", "documents")}&maxResults=50&page={str(page_num)}'


def get_html_page(url: str) -> str:
    logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    try:
        page = requests.get(url)
        return page.text
    except Exception as e:
        logger.error(f'Error while retrieving {url} : {e}')
        return '<html></html>'


def get_contract_notice_id_from_html(url: str, html_parser) -> str:
    html = etree.parse(StringIO(get_html_page(url)), html_parser)
    element = html.xpath(HTML_NOTICE_ID_XPATH)
    return normalize_notice_id(element[0]) if len(element) else ''


def get_publication_date_from_html(html: _Element) -> str:
    publication_date = html.xpath("//span[@id='cft.documents.filter.publication_date']/text()")
    return publication_date[0] if len(publication_date) else ''


def process_procurement_page(url: str, html_parser: HTMLParser(), current_page: int, nb_pages: int,
                             contract_id: str) -> None:
    logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    input_bucket = utils.get_env_var_value("INPUT_BUCKET")
    logger.info(f'Parsing page {current_page}/{nb_pages}...')
    html = etree.parse(StringIO(get_html_page(get_document_url(url, current_page))), html_parser)
    download_urls = html.xpath(DOWNLOAD_DOCS_XPATH)
    publication_date = get_publication_date_from_html(html)
    if len(download_urls) and publication_date:
        for download_url in download_urls:
            file, filename = download_file_as_bytes(download_url)
            if file:
                path = get_procurement_output_path(filename, publication_date, contract_id)
                write_file_in_s3(input_bucket, path, file)
    else:
        logger.info(f'No relevant document found on this page.')


def process_procurement_url(url: str, html_parser: HTMLParser()) -> None:
    logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    logger.info(f'Processing procurement with URL : {url}')
    contract_id = get_contract_notice_id_from_html(url, html_parser)
    html = etree.parse(StringIO(get_html_page(get_document_url(url, 1))), html_parser)
    nb_documents = get_nb_documents(html)
    nb_pages = int(math.ceil(nb_documents / 50))
    current_page = 1
    while current_page <= nb_pages:
        process_procurement_page(url, html_parser, current_page, nb_pages, contract_id)
        current_page += 1
    remove_resources_from_ingestion_table(url)


def process_procurement_urls(procurement_urls: List[str]):
    logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    logger.info(f'Start processing {len(procurement_urls)} eTendering URLs')
    try:
        for url in procurement_urls:
            html_parser = etree.HTMLParser()
            process_procurement_url(url, html_parser)
    except Exception as err:
        logger.error(f'Error while downloading procurement documents, URLs kept in the ingestion table : {err}')
