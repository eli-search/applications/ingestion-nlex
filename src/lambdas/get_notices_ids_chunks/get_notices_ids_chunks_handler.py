import math
import os

from src.common.aws_requests import get_ingestion_checkpoint
from src.common.config import MAX_NB_PAGES_TO_PROCESS
from src.common.config import MAX_PAGE_SIZE
from src.common.exceptions import ScanTableException
from src.common.exceptions import TableNotFoundException
from src.common.log import create_logger
from src.common.models import NoticeType
from src.common.ted_client import get_notices_from_ted_api
from src.common.utils import get_api_filters
from src.common.utils import get_end_date
from src.common.utils import get_env_var_value
from src.common.utils import get_start_date

os.environ["LOGGER_NAME"] = "get-notices-ids-chunks-handler"
logger = create_logger(get_env_var_value("LOGGER_NAME"))


def get_number_of_notices(start_date: str, end_date: str, notice_type: NoticeType) -> int:
    response = get_notices_from_ted_api(get_api_filters('ND', 1, 1, start_date, end_date, notice_type))
    return response["total"] if response["total"] else 0


def handle(_event, _context) -> dict:
    try:
        logger.info(f'Method get_notice_ids_chunks triggered with parameters : {_event}')
        notice_type = NoticeType(_event['notice_type'])
        start_date = get_start_date(get_ingestion_checkpoint(notice_type.value), logger)
        end_date = get_end_date()
        chunks = []
        if start_date <= end_date:
            nb_notices = get_number_of_notices(start_date, end_date, notice_type)
            if nb_notices:
                nb_pages = int(math.ceil(nb_notices / MAX_PAGE_SIZE))
                chunks = [
                    {"start_page": x, "end_page": min(x + MAX_NB_PAGES_TO_PROCESS - 1, nb_pages)}
                    for x in range(1, nb_pages, MAX_NB_PAGES_TO_PROCESS)
                ]
                logger.info(f'{len(chunks)} chunks created to process {nb_pages} pages')
        return {"statusCode": 200, "chunks": chunks}
    except TableNotFoundException as err:
        raise err
    except ScanTableException as err:
        raise err
    except Exception as err:
        logger.error(f'Error during the creation of notice ids chunks, ingestion checkpoint not updated : {err}')
