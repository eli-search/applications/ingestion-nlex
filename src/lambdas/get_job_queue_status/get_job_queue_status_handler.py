import os
from typing import Dict

from src.common.aws_requests import get_queue_messages_status
from src.common.log import create_logger
from src.common.utils import get_env_var_value

os.environ["LOGGER_NAME"] = "get-job-queue-status-handler"
logger = create_logger(get_env_var_value("LOGGER_NAME"))


def handle(_event, _context) -> Dict:
    logger.info(f'Method ingest_resources triggered with parameters : {_event}')
    try:
        queue_message_status = get_queue_messages_status()
        logger.info(f"Current queue status: {queue_message_status}")
        number_of_messages = queue_message_status.get('ApproximateNumberOfMessages', -1)
        number_of_messages_visible = queue_message_status.get('ApproximateNumberOfMessagesNotVisible', -1)
        number_of_messages_delayed = queue_message_status.get('ApproximateNumberOfMessagesDelayed', -1)
        process_completed = not int(number_of_messages) and \
                            not int(number_of_messages_visible) and \
                            not int(number_of_messages_delayed)

        response = {
            'ingestion_process_completed': process_completed,
            'number_of_messages': number_of_messages,
            'number_of_messages_visible': number_of_messages_visible,
            'number_of_messages_delayed': number_of_messages_delayed
        }
        return {
            "statusCode": 200, "body": response
        }
    except Exception as err:
        logger.error(f'Error during the sync with SQS queue message status: {err}')
