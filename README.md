# TED AI - notice ingestion pipeline

## Description

![Ingestion pipeline diagram](./doc/ingestion-full.png)

This pipeline downloads notices from TED using the Web API and procurement data from eTendering after being triggered by a scheduled event from 
Amazon EventBridge. This data is then copied in an Amazon S3 bucket.

There are 5 sequential phases in the pipeline:

- Retrieval of Contract Notices (TED data)
- Retrieval of Prior Information Notices (TED data)
- Retrieval of Contract Award Notices (TED data)
- Retrieval of Change Notices (TED data)
- Retrieval of Procurement data (eTendering)

Please note that the retrieval of the 4 types of notices is the same so this process is not repeated in the diagram.

The ingestion is composed of 3 DynamoDB tables:

- Ingestion checkpoint table: keep track of the latest processing date; used to start retrieving notices from that date
- Ingestion tasks table: list of notice IDs to be processed
- Ingestion references table: contains the links between the different types of notices

The ingestion is tackled by serverless tasks such as Lambda (short running tasks) and ECS tasks (long-running tasks):

- Get Notices IDs (Lambda): Used to retrieve the IDs of the notices to download
- Ingest Resources (ECS: https://code.europa.eu/ted-ai/applications/ingestion-ecs): Used to insert the resources IDs to ingest into the ingestion SQS Queue
- Download Resources (Lambda): Used to download the XML files and procurement documents and upload them to S3
- Get Job Queue Status (Lambda): Used to retrieve the status of the SQS Queue, to check if the process is finished or not

The synchronization of tasks, for example checking the status of a phase, is done using flags and conditional steps in Step Functions.

## Prerequisites

### Libraries & tools
- Python 3.10.9
- Node 19.9.0
- npm 9.6.4

### Modules 
- The project https://code.europa.eu/ted-ai/applications/ingestion-ecs must be tagged and deployed as a docker in the Elastic Container Registry of the environment via the CI pipeline.
- The project https://code.europa.eu/ted-ai/infrastructure/TEDAI_IAC must be deployed with the reference of the Docker image in ECR to set it up and make it ready for the ingestion pipeline.

## Install serverless 

```shell
npm install  serverless
```

## Install python requirements in a virtual environment

```shell
python -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
```

## Deploy from the local environment

1. Set an AWS profile having the rights to deploy in the environment and set the correct region

```shell
export AWS_PROFILE=tedai-dev
export AWS_REGION=eu-west-1
```

2. Install the required plugins

```shell
sudo serverless plugin install -n serverless-step-functions --stage <stage>
sudo serverless plugin install -n serverless-plugin-utils --stage <stage>
sudo serverless plugin install -n serverless-python-requirements --stage <stage>
sudo serverless plugin install -n serverless-deployment-bucket --stage <stage>
```

3. Deploy the serverless project

```shell
serverless deploy --stage <stage>
```